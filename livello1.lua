
--Inizializza il composer
local composer = require("composer")
--Crea una nuova scena nel composer
local scene = composer.newScene() --creazione della nuova scena

local physics = require("physics")
	physics.setDrawMode("normal")
	physics.start(true)
	physics.setGravity(0,0)
	--physics.setAverageCollisionPositions( true )

-- variabili -----
local background, leftBarrier, rightBarrier, bg, fg, albero1, albero2, arrowLeft, arrowRight, arrowUp, arrowDown, button

-- METODI -------------

local function syringeLoop()
	print(#syringes)
	for i,thisSyringe in ipairs(syringes) do
		if thisSyringe.x >= 1950 then
			display.remove(thisSyringe)
			table.remove(syringes,i)
		end
   end		
end
	

local timer = timer.performWithDelay(1000,syringeLoop,0)

local function puncture(event)
	local syringe = display.newSprite(fg,siringaSheet,seqs_m)
	syringe:toBack()
	syringe.x = runningDoctor.x
	syringe.y = runningDoctor.y
	syringe:play()
	table.insert(syringes,syringe)
	--if keyPress (arrowRight)==true then
	   local puncture = transition.to(syringe,{time=700,x=1950})
	--else
	--    local puncture = transition.to(syringe,{time=800,x=-1950})
    --end
end

--runningDoctor.isFixedRotation=true
-- We pause the sprite animation
runningDoctor:pause()
-- Five actions are possiblee
--    stand: badguy is not moving
--    moveLeft: badguy is movingLeft
--    moveRight: badguy is movingRight
--    Jump: badguy is jumping
--    fall: badguy is falling from a platform

-- initially, the badGuy is not moving: i.e. we set the user-defined action property to "stand"
runningDoctor.action="stand"
-- Moreover, we set the initial runningDoctor speed to 0

--runningDoctor.speed = 6

--COLLISIONE DOTTORE
local function onCollision(self,event)
	 if event.phase=="began" then
	 	if event.other.name == "background"  or  event.other.name == "albero"  
	 		or  event.other.name == "albero1"  or  event.other.name == "albero2" then
		   -- calculate the bottom of the badguy and the top of the other object...
           local selfBottom =self.y+self.height/2
           local collidedObjectTop =event.other.y-event.other.height/2
		   -- if badguy is on the other object...
           if collidedObjectTop>selfBottom then 
			    -- this means that badguy landed on the ground or on a platform  
				-- so, set its action to "stand" and pause the running sprite animation  
				-- and set its speed to 0
		        self.action="stand"	
				runningDoctor.speed = 0
	 		    runningDoctor:pause()
           end
	  end	
  -- if the collision between badGuy and another object has ended...	  	
  elseif event.phase=="ended" then
 if event.other.name=="albero" or  event.other.name == "albero"  
	 		or  event.other.name == "albero1"  or  event.other.name == "albero2"then
	   -- this means that badguy is falling so
	   -- pause the running animation and change its action to "fall"
	   -- set the horitzonal speed to 0
	   runningDoctor:pause()	
	  -- runningDoctor.action="movedown"
	   runningDoctor.speed=0
	 end		 	 		
  end	   
  return true
end


--FINE COLLISONE DOTTORE


-- this function is a touch listener that moves runningDoctor
local function moverunningDoctor(event)
	-- select the touched arrow
	local arrow = event.target
	  -- if the touch event has started..
      if event.phase=="began"  then
		  -- give the focus to the current touched arrow
		  display.getCurrentStage():setFocus(arrow)
          
		   -- if runningDoctor is not jumping or falling...
    	   
    	   if runningDoctor.action ~= "moveup" and runningDoctor.action ~= "movedown" then
	 		  -- set runningDoctor speed to 4 (that  is, 4 pixel per frame)
	 		  runningDoctor.speed = 6
			
			    -- ... and the left arrow has been touched	
		      if arrow.name=="left" then	
  			     -- set the runningDoctor animation sequence to "runLeft"
				 runningDoctor:setSequence("runLeft")
				 -- set its action to "moveLeft"
			     runningDoctor.action="moveLeft"		
	             -- otherwise if the right arrow has been touched...		 	 		 
 	           elseif arrow.name=="right" then 
				    -- set the runningDoctor animation sequence to "runRight"
				   runningDoctor:setSequence("runRight")
				   -- set its action to "moveRight"
			       runningDoctor.action="moveRight"	  

			   elseif arrow.name=="up" then 
				    -- set the runningDoctor animation sequence to "runRight"
				   runningDoctor:setSequence("runUp")
				   -- set its action to "moveRight"
			       runningDoctor.action="moveUp"

     		   elseif arrow.name=="down" then 
				     --set the runningDoctor animation sequence to "runDown"
			       runningDoctor:setSequence("runDown")
				     --set its action to "moveDown"
			       runningDoctor.action="moveDown"	 

	           end
		  end   
					
	    elseif event.phase=="ended"  then
			-- if arrow touch has ended
			-- free the touch focus
			display.getCurrentStage():setFocus(nil)
		    
		    -- set the runningDoctor action to stand if he's not falling or jumping
			-- and then  pause its running animation a set his speed to 0
			if runningDoctor.action~="moveup" and runningDoctor.action~="movedown" then
			  runningDoctor.action="stand"
			end   
			runningDoctor:pause()
			-- set its speed to 0
			runningDoctor.speed=0
	  end	 
 	return true
end	

-- this enterFrame listener, at every frame, 
-- moves and animates badguy if
-- its associated action is moveLeft or moveRight
function applyContinuosMovingSpeed(event)   
	   if runningDoctor.action=="moveLeft" then 
		   if runningDoctor.isPlaying == false then	
			 runningDoctor:play()
		   end	 	 
		   runningDoctor.x=runningDoctor.x-runningDoctor.speed	 
	   elseif runningDoctor.action=="moveRight" then
		   if runningDoctor.isPlaying == false then	
			 runningDoctor:play()
		   end
			runningDoctor.x=runningDoctor.x+runningDoctor.speed						
	    elseif runningDoctor.action=="moveUp" then
		   if runningDoctor.isPlaying == false then	
			 runningDoctor:play()
		   end
			runningDoctor.y=runningDoctor.y-runningDoctor.speed		
        elseif runningDoctor.action=="moveDown" then
		    if runningDoctor.isPlaying == false then	
	     	 runningDoctor:play()
		      end
			runningDoctor.y=runningDoctor.y+runningDoctor.speed		
        
           end		 
	return true
end		

function scene:create( event ) --quando la scena è nascosta
    local sceneGroup = self.view
	
		background = display.newImageRect(bg,"img/livello/sfondo.jpg",1920,1080)
		leftBarrier = display.newRect(0,0,1,960)
		rightBarrier = display.newRect(0,0,1,960)
		albero1 = display.newImageRect(fg,"img/livello/albero1.png",300,400)
		albero2 = display.newImageRect(fg,"img/livello/albero2.png",250,350)
		
		arrowLeft = display.newImageRect("img/arrowLeft.png",80,80)
		arrowRight = display.newImageRect("img/arrowRight.png",80,80)
		arrowUp = display.newImageRect("img/arrowUp.png",80,80)
		arrowDown = display.newImageRect("img/arrowDown.png",80,80)
		
		button = display.newImageRect("img/pulsante.png",150,150) 
		
	 		-- loading and placement of the bad guy sprite 
		local opt = { width = 383, height = 400, numFrames = 6}
		local seq_doctor = graphics.newImageSheet("img/doctor.png", opt)
		local seqs ={{ name = "runLeft",start = 4,count = 3,time = 300,loopCount = 1,loopDirection ="forward"},
			 {name = "runRight",start = 4,count = 3,time = 300,loopCount = 1,loopDirection ="forward"},
			 {name = "runUp",start = 4,count = 3,time = 300,loopCount = 1,loopDirection ="forward"},
			 {name = "runDown",start = 4,count = 3,time = 300,loopCount = 1,loopDirection ="forward"},
             {name = "start",start = 3,count = 3,time = 300,loopCount = 1,loopDirection ="forward"}
			} 
		
		local runningDoctor=display.newSprite(seq_doctor,seqs)
		
		--ANIMAZIONE SIRINGA

		local opt_m = { width = 44, height = 32, numFrames = 1}
		local siringaSheet = graphics.newImageSheet("img/siringa.png", opt_m)
		local seqs_m ={{
	          name = "shoot",
			  start = 1,
              count = 1,
              time = 300,
			  loopCount = 0,
			  loopDirection ="forward"
	    	 }
			} 

end --fine funzione scene:create

function scene:show( event ) --quando la scena è nascosta
    local sceneGroup = self.view
    local phase = event.phase
    if ( phase == "will" ) then --quando sta per essere nascosta
	
		background.x=display.contentCenterX
		background.y=display.contentCenterY
		background.name="background"
		
		leftBarrier.x = 0
		leftBarrier.y = display.contentCenterY
		leftBarrier.name="barrier"
		
		rightBarrier.x = 1920
		rightBarrier.y = display.contentCenterY
		rightBarrier.name="barrier"
		
		albero1.x=display.contentWidth-300
		albero1.y=display.contentHeight-1100
		albero1.name="albero"
		
		albero2.x=display.contentWidth-1400
		albero2.y=display.contentHeight-900
		albero.name="albero2"
		
		runningDoctor.x = 150
        runningDoctor.y = display.contentHeight-540
		runningDoctor:setSequence("start")
		
				-- FRECCE PER IL MOVIMENTO (DOCTOR)
		arrowLeft.x = display.contentWidth-350
		arrowLeft.y = display.contentHeight-150
		arrowLeft.name = "left"
		arrowRight.x = display.contentWidth-150
		arrowRight.y = display.contentHeight-150
		arrowRight.name = "right"
		arrowUp.x = display.contentWidth-250
		arrowUp.y = display.contentHeight-250
		arrowUp.name = "up"
		arrowDown.x = display.contentWidth-250
		arrowDown.y = display.contentHeight-70
		arrowDown.name = "down"

--FINE FRECCE PER IL MOVIMENTO (DOCTOR)

		button.x = display.contentWidth-1800
		button.y = display.contentHeight-150
		
		physics.addBody(leftBarrier,"static",{friction=1.0,bounce=0.0,density=1.8})
		physics.addBody(rightBarrier,"static",{friction=1.0,bounce=0.0,density=1.8})
		physics.addBody(albero, "static",{friction=1.0,bounce=0.0,density=1.8} ) 
		physics.addBody(albero1, "static",{friction=1.0,bounce=0.0,density=1.8} ) 
		physics.addBody(albero2, "static",{friction=1.0,bounce=0.0,density=1.8} ) 
		physics.addBody(runningDoctor,"dynamic",{friction=1.0,bounce=0.0,density=1.3})
		
		bg = display.newGroup()
        fg = display.newGroup()
		
    elseif ( phase == "did" ) then --quando è stata nascosta

		Runtime:addEventListener("enterFrame",applyContinuosMovingSpeed)
		button:addEventListener("tap",puncture)
		runningDoctor.collision=onCollision -- Activate the local collision listener for badGuy  
		runningDoctor:addEventListener("collision",runningDoctor)  
		arrowRight:addEventListener("touch",moverunningDoctor) --activate the touch listener moverunningDoctor on arrows Images
		arrowLeft:addEventListener("touch",moverunningDoctor)	
		arrowUp:addEventListener("touch",moverunningDoctor)	
		arrowDown:addEventListener("touch",moverunningDoctor)

    end --fine controllo fase d'evento
end --fine funzione scene:show


-- activate the enterFrame Listener applyContinuosMovingSpeed

-- END: IMPLEMENTATION OF  BADGUY MOVEMENTS

--Ascoltatori scena-------------------------------------------------------------
scene:addEventListener( "create", scene ) --creata
scene:addEventListener( "show", scene ) --mostrata
--------------------------------------------------------------------------------

return scene

	

