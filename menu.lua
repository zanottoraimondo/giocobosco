--Inizializza il composer
local composer = require("composer")
--Crea una nuova scena nel composer
local scene = composer.newScene() --creazione della nuova scena
--Gruppi
local background = display.newGroup() --gruppo sfondo
local touch = display.newGroup() --gruppo "toccabile"

--variabili
local BG, button_play, button_option

--Funzioni di scena-------------------------------------------------------------
function scene:create(event) --la scena viene creata ma non è visualizzata
  local sceneGroup = self.view --imposta la visione sul gruppo di scena
  --carico le immagini nelle variabili
  BG = display.newImageRect(background, "img/menu/bgMenu.jpg", 656, 373)
  button_play = display.newImageRect(touch, "img/menu/giocaRed.png", 213, 115)
  button_option = display.newImageRect(touch, "img/menu/impostazioniRed.png", 230, 66)
  --aggiunta dei gruppi precedenti al gruppo di scena
  sceneGroup:insert(background)
  sceneGroup:insert(touch)
end --fine funzione scene:create

function scene:show(event) --quando la scena viene mostrata
  local sceneGroup = self.view
  local phase = event.phase --estrapola la fase d'evento

  if (phase == "will") then --quando sta per esere mostrata
    --gestione coordinate delle immagini
    BG.x = display.contentCenterX
    BG.y = display.contentCenterY
    button_play.x = display.contentCenterX + 200
    button_play.y = display.contentCenterY - 70
	button_option.x = display.contentCenterX + 200
    button_option.y = display.contentCenterY + 20
  elseif (phase == "did") then --quando la scene è mostrata
    --controllo delle variabili globali del composer
    if (composer.getVariable("music") == false) then --se la variabile musica è false
      audio.setVolume(0, {channel = 1}) --imposta il volume a 0
    else
      audio.setVolume(1, {channel = 1}) --altrimenti mantieni il volume a 1
    end--fine if controllo variabile
    --Ascoltatori
    button_play:addEventListener("tap", giocascelto) --tap gioca
  end --fine if controllo fase d'evento
end--fine funzione scene:show

function scene:destroy( event )
    local sceneGroup = self.view
    --quando la scena è distrutta
end --fine funzione scene:destroy

--Funzioni di gioco-------------------------------------------------------------
function giocascelto() --funzione per passare al gioco
  composer.removeScene("menu") --distruggi scena menu.lua
  composer.gotoScene("livello1") --carica scena livello1.lua
end --fine funzione giocascelto

function impostazioni() --funzione per passare al gioco
	  audioOn = display.newImageRect(touch, "img/menu/audioOn.png", 213, 115)
	  audioOff = display.newImageRect(touch, "img/menu/audioOff.png", 213, 115)
end --fine funzione giocascelto


--Ascoltatori scena-------------------------------------------------------------
scene:addEventListener( "create", scene ) --creata
scene:addEventListener( "show", scene ) --mostrata
scene:addEventListener( "destroy", scene ) --distrutta
--------------------------------------------------------------------------------

return scene
